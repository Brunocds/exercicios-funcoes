data = input('Digite uma data no formato DD/MM/AAAA: ')

def retornar_data_mes_por_extenso(data):
    #validação da data

    if len(data)!=10:
        return "Null"

    if data[2]!="/" or data[5]!="/":
        return "Null"

    dia, mes, ano = map(int, data.split('/'))
    # checando se o mês está certo
    if (mes < 1 or mes > 12):
        validacaomes = False
    else:
        validacaomes = True

    # checando o dia para fevereiro
    if mes == 2:
        # checando ano bissexto
        if ano % 4 == 0 and ano % 100 != 0 or ano % 400 == 0:
            if dia > 0 and dia < 30:
                validacaodia = True
            else:
                validacaodia = False
        else:
            if dia > 0 and dia < 29:
                validacaodia = True
            else:
                validacaodia = False
                # checando o dia para os meses que tem 31 dias ou 30 dias
    elif (mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12):
        if dia > 0 and dia < 32:
            validacaodia = True
        else:
            validacaodia = False
    elif (mes == 4 or mes == 6 or mes == 9 or mes == 11):
        if dia > 0 and dia < 31:
            validacaodia = True
        else:
            validacaodia = False
    else:
        validacaodia = False

    # checando o ano
    if ano >= 0:
        validacaoano = True
    else:
        validacaoano = False

    #caso o dia, mes ou ano seja invalido, retorna Null
    if not(validacaodia and validacaomes and validacaoano):
        return "Null"

    #criação de um de-para para transformar o mês por extenso
    de_para_mes = {1: 'janeiro', 2: 'fevereiro', 3: 'março', 4: 'abril', 5: 'maio', 6: 'junho', 7: 'julho', 8: 'agosto', 9: 'novembro', 10: 'outubro', 11: 'novembro', 12: 'dezembro'}
    mes_por_extenso = de_para_mes[mes]

    data_por_extenso = str(dia)+" de "+mes_por_extenso+" de "+str(ano)

    # retorno da entrada
    return data_por_extenso

retornar_data_mes_por_extenso(data)
