import random

palavra = input('Digite uma palavra para ser embaralhada: ')


def embaralha_palavra(palavra):
     letras = []
     for i in range(len(palavra)):
         letras.append(palavra[i])
     palavra_embaralhada = ""
     for i in range(len(palavra)):
         letra = random.choice(letras)
         palavra_embaralhada += letra
         letras.remove(letra)
     return palavra_embaralhada.lower()

print(embaralha_palavra(palavra))

