import random
import time

input("Digite qualquer valor para jogar seus dados: ")

dado_1 = random.randint(1,6)
dado_2 = random.randint(1,6)

soma = dado_1 + dado_2

print(f"Você tirou o {dado_1} no primeiro dado e {dado_2} no segundo dado\n\nEstamos calculando o resultado, aguarde um momento!")

for i in range(5):
    time.sleep(1)
    print(".", end="")

print(f"\n\nA soma dos seus dados é: {soma}")

if (soma==7 or soma==11):
    print("\nVocê ganhou! :)")
elif (soma==2 or soma==3 or soma==12):
    print("\nVocê perdeu! Mas não deve nada para a Nat e o Groger")
else:
    print("\nVocê perdeu e agora deve R$10.000 para Nat e o Groger!")

